Concrete runner is a little docker container for doing basic CI.

Run as follows:

    docker run -d presentthinkingltd/concrete-runner \
    -e GIT_REPO='<repourl>' \
    -e GIT_BRANCH='<branch>'' \
    -e BUILD_COMMAND='<build command>' 

## TODO 

Add support for authentication, and possibly get concrete to watch the repo for changes
