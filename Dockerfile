from    ubuntu:12.04
run     echo "deb http://archive.ubuntu.com/ubuntu precise main universe" > /etc/apt/sources.list
run     apt-get -y update
run     apt-get -y install wget git python-setuptools
run     easy_install docker_py
run     wget -O - http://nodejs.org/dist/v0.10.25/node-v0.10.25-linux-x64.tar.gz | tar -C /usr/local/ --strip-components=1 -zxv
run     npm install concrete -g --registry http://registry.npmjs.eu/
run     apt-get -y install mongodb-server
add     runner.sh /runner.sh
run     chmod +x /runner.sh
EXPOSE 4567
ENTRYPOINT /runner.sh