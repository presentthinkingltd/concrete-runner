#!/bin/bash
cd /root
mkdir workspace
cd /root/workspace
mongod --fork --logpath /tmp/mongo.log
git clone $GIT_REPO project
cd project
git config --add concrete.branch $GIT_BRANCH
git config --add concrete.runner $BUILD_COMMAND
concrete .